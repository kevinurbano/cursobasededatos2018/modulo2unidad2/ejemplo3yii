<?php

namespace app\controllers;

use Yii;
use app\models\Tenistas;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TenistasController implements the CRUD actions for Tenistas model.
 */
class TenistasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tenistas models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Tenistas::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tenistas model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
				
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
	
	public function actionTodos()
    {
		// select * from tenistas;
		$consulta = Tenistas::find()
							  ->all();	
				
		$consultaArray = Tenistas::find()
								   ->asArray()
								   ->all();			
		
		// select * from tenistas where id=2 or id=3;
		//$datos = Tenistas::findAll([2,3]);		
		//var_dump($datos);
		
        
		return $this->render('todos', [
            'datos' => $consulta,
			'datos1' => $consultaArray,
        ]);
    }
	
	public function actionTodos1()
	{
		//Variable que apunta a la bbdd
		$conexion=Yii::$app->db;
		//Solo para consultas de seleccion
		$datos=$conexion->createCommand("Select * from tenistas")->queryAll();
		
		return $this->render('todos1', [
            'datos' => $datos,			
        ]);
	}	
	

    /**
     * Creates a new Tenistas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tenistas();
		// Carga los datos del formulario y se los asigno al modelo
		$model -> load(Yii::$app->request->post());
		// Almacenar los datos del modelo en la tabla
		$model -> save();
		
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Tenistas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tenistas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tenistas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tenistas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tenistas::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionConsulta1() {
        // Mostrar todos los tenistas con id='2'
        $resultados = tenistas::find()
                     ->where(['id' => 2])
                     ->all();
        
        return $this->render('consultas', [
            'datos' => $resultados,
            'titulo' => 'Consulta1',
            'texto' => 'Mostrar todos los tenistas con id 2'
        ]);
    }

    public function actionConsulta2() {
        // Listar todos los tenistas ordenado por nombre
        $resultados = tenistas::find()
                      ->orderBy('nombre')
                      ->all();
        
        return $this->render('consultas', [
            'datos' => $resultados,
            'titulo' => 'Consulta2',
            'texto' => 'Mostrar todos los tenistas ordenado por nombre'
        ]);
    }
    
    public function actionConsulta3() {
        // Listar el id y el nombre de los tenistas
        $resultados = tenistas::find()
                      ->select('id,nombre')
                      ->all();
        
        return $this->render('consultas', [
            'datos' => $resultados,
            'titulo' => 'Consulta3',
            'texto' => 'Mostrar el id y el nombre de los tenistas'
        ]);
    }
    
    public function actionConsulta4() {
        // Listar los tenistas cuyo nombre empiece por N o cuya edad es menor que 40
        $resultados = tenistas::find()
                      ->where("nombre like 'n%' or edad<40")
                      ->all();
        
        return $this->render('consultas', [
            'datos' => $resultados,
            'titulo' => 'Consulta4',
            'texto' => 'Mostrar tenistas cuyo nombre empiece por N o cuya edad es menor que 40'
        ]);
    }

}
