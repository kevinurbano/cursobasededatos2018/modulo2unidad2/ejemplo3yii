<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tenistas */

$this->title = 'Actualizando datos del Tenista: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Tenistas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<div class="tenistas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
