<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Naciones */

$this->title = 'Crear Naciones';
$this->params['breadcrumbs'][] = ['label' => 'Naciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="naciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
